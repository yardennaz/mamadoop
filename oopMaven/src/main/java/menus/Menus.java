package menus;

import utils.ConsoleHandler;

public class Menus {
  private static final String EXIT_CODE = "EXIT";
  private static final ConsoleHandler<String> consoleHandler = new ConsoleHandler<>();

  public static void printWelcomingMessage() {
    Menus.consoleHandler.print(
            "----   welcome   -----\n----- "
            + Menus.EXIT_CODE
            + ": exit -----");
  }

  public static void printLogin() {
    Menus.consoleHandler.print(
            """
                    -- enter personal number --
                    ----- enter password ------
                    """);
  }

  public static void printSignUp() {
    Menus.consoleHandler.print(
            """
                    
                    enter info in the following order:personal number
                    name
                    speciality type
                    speciality level
                    password
                    password again
                    usertype
                    """);
  }

  public static void newCourseSignup() {
    Menus.consoleHandler.print(
            """
                    enter number of the course
                    you would like to subscribe to:
                    """);
  }

  public static void guideACourse() {
    Menus.consoleHandler.print(
            """
                    enter number of the course
                    you would like to guide pls:
                    """
    );
  }

  public static void changePermissions() {
    Menus.consoleHandler.print(
            """
                    enter number of the user
                    you would like to change permissions:
                    """
    );
  }

  public static void currentLearnings() {
    Menus.consoleHandler.print("""
            currently learning:
            """);
  }

  public static void addLearnings() {
    Menus.consoleHandler.print("""
            start learning:
            """);
  }
}
