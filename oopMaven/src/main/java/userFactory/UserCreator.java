package userFactory;

import users.SpecialityTypes;
import users.User;

@FunctionalInterface
public interface UserCreator {
    public User getUser(String personalNum, String password, String name, SpecialityTypes type, String level, String job);
}
