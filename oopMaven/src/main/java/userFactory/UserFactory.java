package userFactory;

import exceptions.InvalidParameters;
import users.*;

import java.util.Hashtable;

public class UserFactory {
  private static UserFactory userFactory = null;

  private static Hashtable<String, UserCreator> users;

  private UserFactory() {
    UserFactory.users = new Hashtable<>();
    UserFactory.addUser(
        (String personalNum, String password, String name, SpecialityTypes type, String level, String job) ->
            new Manager(personalNum, password, name, type, level, job),
        "MANAGER");
    UserFactory.addUser(
        (String personalNum, String password, String name, SpecialityTypes type, String level, String job) ->
            new Guide(personalNum, password, name, type, level, job),
        "GUIDE");
    UserFactory.addUser(
        (String personalNum, String password, String name, SpecialityTypes type, String level, String job) ->
            new Student(personalNum, password, name, type, level, job),
        "STUDENT");
  }

  public static UserFactory getInstance() {
    if (UserFactory.userFactory == null) UserFactory.userFactory = new UserFactory();

    return UserFactory.userFactory;
  }

  public static void addUser(UserCreator func, String key) {
    UserFactory.users.put(key, func);
  }

  public User createUser (
      String key, String personalNum, String password, String name, SpecialityTypes type, String level) throws InvalidParameters {
    try {
      return UserFactory.users.get(key).getUser(personalNum, password, name, type, level, key);
    } catch (Exception e) {
      throw new InvalidParameters("bad parameters for creation");
    }
  }
}
