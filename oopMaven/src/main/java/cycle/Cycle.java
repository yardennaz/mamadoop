package cycle;

import courses.Course;
import courses.Participant;
import lombok.Getter;
import lombok.Setter;
import users.User;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

@Getter @Setter
public class Cycle implements Serializable {
    private final long serialVersionUID = 1L;
    private int id;
    private Course course;
    private ArrayList<Participant> Participants;
    private User guide = null;
    private Date startingDate;

    public Cycle(int id, Course course, ArrayList<Participant> Participants, Date startingDate) {
        this.course = course;
        this.Participants = Participants;
        this.startingDate = startingDate;
        this.id = id;
    }

    @Override
    public String toString() {
        return "Cycle\n" +
                "course=" + course +
                ", participants=" + Participants.toString() +
                ", guide=" + guide +
                ", startingDate=" + startingDate;
    }
}
