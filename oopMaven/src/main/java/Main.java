import FileHandler.FileWriter;
import courses.*;
import cycle.Cycle;
import org.checkerframework.checker.units.qual.C;
import userActions.ActionSupplier;
import users.*;
import utils.ConsoleHandler;
import utils.CreateHash;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class Main {
  public static void main(String[] args) {
    final String EXIT_CODE = "0";
    final ConsoleHandler<String> writer = new ConsoleHandler<>();
    ActionSupplier.initActions();
    //
    Guide guide =
        new Guide("a", CreateHash.toHash("a"), "dusa", SpecialityTypes.TIHNUT, "09", "GUIDE");
    Manager manager =
        new Manager(
            "b", CreateHash.toHash("b"), "krashnos", SpecialityTypes.DEVOPS, "09", "MANAGER");
    Student student =
        new Student(
            "c", CreateHash.toHash("c"), "tal green", SpecialityTypes.TIHNUT, "02", "STUDENT");
    ArrayList<User> users = new ArrayList<>();
    users.add(guide);
    users.add(manager);
    users.add(student);

    ArrayList<Course> courses = new ArrayList<>();

    Calendar c = Calendar.getInstance();
    c.setTime(new Date());
    c.add(Calendar.DATE, 1);

    SpecificSpecCourse specificSpecCourse =
        new SpecificSpecCourse(
            "nice course",
            10,
            "the coolest course",
            new SpecialityTypes[] {SpecialityTypes.TIHNUT, SpecialityTypes.DEVOPS});
    StepCourse stepCourse = new StepCourse("bad course", 15, "if you fail you out", "05", "09");
    TestCourse testCourse = new TestCourse("smart course", 20, "have to be smart", 60);
    courses.add(stepCourse);
    courses.add(testCourse);
    courses.add(specificSpecCourse);

    ArrayList<Cycle> cycles = new ArrayList<>();
    Cycle cycle = new Cycle(1, specificSpecCourse, new ArrayList<>(), c.getTime());
    Cycle cycle1 = new Cycle(2, stepCourse, new ArrayList<>(), c.getTime());

    ArrayList<Participant> participants = new ArrayList<>();
    participants.add(new Participant(manager));
    participants.add(new Participant(student));
    participants.add(new Participant(guide));

    c.add(Calendar.DATE, -3);
    Cycle cycle2 = new Cycle(3,testCourse, participants, c.getTime());

    cycles.add(cycle);
    cycles.add(cycle1);
    cycles.add(cycle2);
    FileWriter<ArrayList<User>> fileWriter = new FileWriter<>("src/main/java/db/users.dat");
    fileWriter.write(users);

    FileWriter<ArrayList<Course>> fileWriter1 = new FileWriter<>("src/main/java/db/courses.dat");
    fileWriter1.write(courses);

    FileWriter<ArrayList<Cycle>> fileWriter2 = new FileWriter<>("src/main/java/db/cycles.dat");
    fileWriter2.write(cycles);
    ActionSupplier.startBase();
  }
}
