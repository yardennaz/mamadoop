package actions;

import lombok.Getter;

@Getter
public class BaseActionClass {
  private final Runnable runnable;

  public BaseActionClass(Runnable runnable) {
    this.runnable = runnable;
  }
}
