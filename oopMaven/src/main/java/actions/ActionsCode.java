package actions;

public enum ActionsCode {
  CREATE("-- CREATE: add new user --"),
  LOGIN("-- LOGIN: login into user --"),
  COURSE_SIGNUP("-- COURSE_SIGNUP: sign up for a new course --"),
  GUIDE_A_COURSE("-- GUIDE_A_COURSE: guide a new course --"),
  CHANGE_PERMISSIONS("-- CHANGE_PERMISSIONS: change the users permission --"),
  NEW_CYCLE("-- NEW_CYCLE: show all the current cycles --"),
  LEARN("-- LEARN: continue learning one of my classes --");

  private final String desc;

  public String desc() {
    return this.desc;
  }

  ActionsCode(String desc) {
    this.desc = desc;
  }
}
