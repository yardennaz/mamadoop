package actions;

public enum Permissions {
  STUDENT("STUDENT"),
  GUIDE("GUIDE"),
  MANAGER("MANAGER");

  private final String name;

  Permissions(String name) {
    this.name = name;
  }
}
