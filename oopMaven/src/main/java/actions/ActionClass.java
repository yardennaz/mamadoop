package actions;

import lombok.Getter;

@Getter
public class ActionClass extends BaseActionClass {
  private final Permissions[] permissions;

  public ActionClass(Permissions[] permissions, Runnable func) {
    super(func);
    this.permissions = permissions;
  }
}
