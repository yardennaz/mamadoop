package userActions;

import FileHandler.FileReader;
import FileHandler.FileWriter;
import actions.ActionsCode;
import annotations.BaseAction;
import exceptions.InvalidParameters;
import menus.Menus;
import userFactory.UserFactory;
import users.SpecialityTypes;
import users.User;
import utils.ConnectedUser;
import utils.ConsoleHandler;
import utils.CreateHash;

import java.util.ArrayList;

public class BaseActions {
  @BaseAction(actionCode = ActionsCode.CREATE)
  public static void createUser() throws InvalidParameters {
    FileWriter<ArrayList<User>> fileWriter = new FileWriter<>("src/main/java/db/users.dat");
    FileReader<ArrayList<User>> fileReader = new FileReader<>("src/main/java/db/users.dat");

    ArrayList<User> allUsers = fileReader.read();
    if (allUsers == null) {
      allUsers = new ArrayList<>();
    }
    Menus.printSignUp();
    try {
      String personalNum = ConsoleHandler.stringInput();
      String name = ConsoleHandler.stringInput();
      SpecialityTypes specialityType = SpecialityTypes.valueOf(ConsoleHandler.stringInput());
      String specialityLevel = ConsoleHandler.stringInput();
      String password = ConsoleHandler.stringInput();
      if (ConsoleHandler.stringInput().equals(password)) {
        try {
          allUsers.add(
              UserFactory.getInstance()
                  .createUser(
                      "STUDENT",
                      personalNum,
                      CreateHash.toHash(password),
                      name,
                      specialityType,
                      specialityLevel));
          fileWriter.write(allUsers);
        } catch (InvalidParameters e) {
          System.out.println(e.getMessage());
        }
      } else {
        throw new InvalidParameters("password did not match");
      }
    } catch (InvalidParameters e) {
      System.out.println(e.getMessage());
    }
  }

  @BaseAction(actionCode = ActionsCode.LOGIN)
  public static void login() {
    Menus.printLogin();
    String personalNum = ConsoleHandler.stringInput();
    String password = ConsoleHandler.stringInput();

    try {
      ConnectedUser.getInstance().attemptConnection(personalNum, password);
      System.out.println("welcome " + ConnectedUser.getInstance().getCurrUser().getName());
      ActionSupplier.startUser();
    } catch (CloneNotSupportedException e) {
      e.printStackTrace();
    } catch (InvalidParameters e) {
      System.out.println(e.getMessage());
    } catch (Exception e) {
      System.out.println("hello");
    }
  }
}
