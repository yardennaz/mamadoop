package userActions;

import FileHandler.FileReader;
import FileHandler.FileWriter;
import actions.ActionsCode;
import actions.Permissions;
import annotations.SetAction;
import courses.Course;
import courses.Participant;
import courses.SpecificSpecCourse;
import courses.StepCourse;
import cycle.Cycle;
import exceptions.AlreadyLearning;
import exceptions.InvalidParameters;
import learn.CurrentLearningCourse;
import learn.LearnCourse;
import menus.Menus;
import userFactory.UserFactory;
import users.User;
import utils.ConnectedUser;
import utils.ConsoleHandler;

import javax.swing.plaf.ColorUIResource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class UserActions {
  private static String DB_PATH = "src/main/java/db/";
  private static String COURSES_PATH = "courses.dat";
  private static String CYCLES_PATH = "cycles.dat";
  private static String USER_PATH = "users.dat";

  private static final FileReader<ArrayList<Course>> courseReader =
      new FileReader<>(DB_PATH + COURSES_PATH);
  private static final FileWriter<ArrayList<Course>> courseWriter =
      new FileWriter<>(DB_PATH + COURSES_PATH);
  private static final FileReader<ArrayList<Cycle>> cycleReader =
      new FileReader<>(DB_PATH + CYCLES_PATH);
  private static final FileWriter<ArrayList<Cycle>> cycleWriter =
      new FileWriter<>(DB_PATH + CYCLES_PATH);
  private static final FileWriter<ArrayList<User>> userWriter =
      new FileWriter<>(DB_PATH + USER_PATH);
  private static final FileReader<ArrayList<User>> userReader =
      new FileReader<>(DB_PATH + USER_PATH);

  @SetAction(
      permissions = {Permissions.GUIDE, Permissions.MANAGER, Permissions.STUDENT},
      actionCode = ActionsCode.COURSE_SIGNUP)
  public static void newCourseSignUp() throws InvalidParameters {
    ArrayList<Cycle> allCycles = UserActions.cycleReader.read();
    Menus.newCourseSignup();
    List<Cycle> futureCycles =
        allCycles.stream()
            .filter(
                (cycle -> {
                  if (cycle
                      .getParticipants().stream().noneMatch(participant -> participant.getUser().getPersonalNum().equals(ConnectedUser.getInstance().getCurrUser().getPersonalNum()))) {
                    if (cycle.getStartingDate().before(Calendar.getInstance().getTime())) {
                      return false;
                    } else if (cycle.getCourse() instanceof SpecificSpecCourse) {
                      return Arrays.stream(
                              ((SpecificSpecCourse) cycle.getCourse()).getCourseSpecialities())
                          .toList()
                          .contains(
                              ConnectedUser.getInstance().getCurrUser().getSpeciality().getType());
                    } else if (cycle.getCourse() instanceof StepCourse) {
                      return ((StepCourse) cycle.getCourse())
                              .getMinSpecialityLevel()
                              .compareTo(
                                  ConnectedUser.getInstance()
                                      .getCurrUser()
                                      .getSpeciality()
                                      .getLevel())
                          <= 0;
                    }

                    return cycle.getStartingDate().before(Calendar.getInstance().getTime());
                  } else {
                    return false;
                  }
                }))
            .toList();
    if (!futureCycles.isEmpty()) {
      futureCycles.forEach(
          (cycle -> System.out.println(futureCycles.indexOf(cycle) + 1 + cycle.toString())));
      try {
        int choice = Integer.parseInt(ConsoleHandler.stringInput()) - 1;
        if (choice > futureCycles.size() || choice < 0) {
          throw new InvalidParameters("there is no " + choice + "course");
        } else {
          allCycles
              .get(allCycles.indexOf(futureCycles.get(choice)))
              .getParticipants()
              .add(new Participant(ConnectedUser.getInstance().getCurrUser()));
          cycleWriter.write(allCycles);
        }
      } catch (InvalidParameters e) {
        System.out.println(e.getMessage());
      } catch (Exception e) {
        System.out.println("enter a number pls");
      }
    } else {
      System.out.println("no courses to sign into");
    }
  }

  @SetAction(
      permissions = {Permissions.MANAGER, Permissions.GUIDE},
      actionCode = ActionsCode.GUIDE_A_COURSE)
  public static void guideACourse() {
    Menus.guideACourse();
    ArrayList<Cycle> allCycles = UserActions.cycleReader.read();
    List<Cycle> noGuideCycles =
        allCycles.stream()
            .filter(
                (cycle -> {
                  if (cycle.getGuide() == null) {
                    if (cycle.getCourse() instanceof SpecificSpecCourse) {
                      return Arrays.stream(
                              ((SpecificSpecCourse) cycle.getCourse()).getCourseSpecialities())
                          .toList()
                          .contains(
                              ConnectedUser.getInstance().getCurrUser().getSpeciality().getType());
                    }
                  }

                  return cycle.getGuide() == null;
                }))
            .toList();
    if (!noGuideCycles.isEmpty()) {
      noGuideCycles.forEach(
          cycle -> System.out.println(noGuideCycles.indexOf(cycle) + 1 + cycle.toString()));
      try {
        int choice = Integer.parseInt(ConsoleHandler.stringInput()) - 1;
        if (choice > noGuideCycles.size() || choice < 0) {
          throw new InvalidParameters("there is no " + choice + "course");
        } else {
          allCycles
              .get(allCycles.indexOf(noGuideCycles.get(choice)))
              .setGuide(ConnectedUser.getInstance().getCurrUser());
          cycleWriter.write(allCycles);
        }
      } catch (InvalidParameters e) {
        System.out.println(e.getMessage());
      } catch (Exception e) {
        System.out.println("enter a number pls");
      }
    } else {
      System.out.println("no courses to guide");
    }
  }

  @SetAction(
      permissions = {Permissions.MANAGER},
      actionCode = ActionsCode.CHANGE_PERMISSIONS)
  public static void changePermissions() {
    ArrayList<User> allUsers = userReader.read();
    Menus.changePermissions();
    List<User> allNonManagerUsers =
        allUsers.stream().filter((user -> !user.getJob().equals("MANAGER"))).toList();
    allNonManagerUsers.forEach(
        user -> System.out.println(allNonManagerUsers.indexOf(user) + 1 + user.toString()));
    try {
      int choice = Integer.parseInt(ConsoleHandler.stringInput()) - 1;
      if (choice > allNonManagerUsers.size() || choice < 0) {
        throw new InvalidParameters("there is no " + choice + " student");
      } else {
        System.out.println("what will be the job of the new user?");
        for (Permissions permission : Permissions.values()) {
          if (!permission.name().equals(allNonManagerUsers.get(choice).getJob())) {
            System.out.println(permission.name());
          }
        }

        String newPermission = ConsoleHandler.stringInput();
        try {
          User oldUser = allNonManagerUsers.get(choice);
          allUsers.add(
              UserFactory.getInstance()
                  .createUser(
                      newPermission,
                      oldUser.getPersonalNum(),
                      oldUser.getPassHash(),
                      oldUser.getName(),
                      oldUser.getSpeciality().getType(),
                      oldUser.getSpeciality().getLevel()));
          allUsers.remove(oldUser);
          userWriter.write(allUsers);
        } catch (Exception e) {
          System.out.println("wrong permission name");
        }
      }
    } catch (InvalidParameters e) {
      System.out.println(e.getMessage());
    } catch (Exception e) {
      System.out.println("enter a number pls");
    }
  }

  @SetAction(
      permissions = {Permissions.MANAGER},
      actionCode = ActionsCode.NEW_CYCLE)
  public static void newCycle() {}

  @SetAction(
      permissions = {Permissions.MANAGER, Permissions.GUIDE, Permissions.STUDENT},
      actionCode = ActionsCode.LEARN)
  public static void learningMenu() throws InvalidParameters{
    CurrentLearningCourse instance = CurrentLearningCourse.INSTANCE;

    if(instance.currentlyLearning()) {
      Menus.currentLearnings();
      System.out.println(instance.getCurrentCourse().toString());
      System.out.println("""
would you like to stop??

YES
NO
""");
      if(ConsoleHandler.stringInput().equals("YES")) {
        instance.stopLearning();
      }
    } else {
      Menus.addLearnings();
      ArrayList<Cycle> allCycles = cycleReader.read();
      List<Cycle> userCycles = allCycles.stream().
              filter(
                      cycle -> cycle.getParticipants().stream()
                              .anyMatch(participant ->
                                      participant.getUser().getPersonalNum().
                                              equals(ConnectedUser.getInstance().
                                                      getCurrUser().getPersonalNum()))
                              && !Calendar.getInstance().getTime().
                              before(cycle.getStartingDate())).toList();
      userCycles.forEach(cycle -> System.out.println(userCycles.indexOf(cycle) + 1 + cycle.toString()));
      try {
        int choice = Integer.parseInt(ConsoleHandler.stringInput()) - 1;
        if (choice > userCycles.size() || choice < 0) {
          throw new InvalidParameters("there is no " + choice + " cycle");
        } else {
          instance.startLearning(new LearnCourse(userCycles.get(choice)));
        }
    } catch (AlreadyLearning e) {
        System.out.println(e.getMessage());
      }
      }
    }
}
