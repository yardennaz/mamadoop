package userActions;

import actions.ActionClass;
import actions.ActionsCode;
import actions.BaseActionClass;
import actions.Permissions;
import annotations.BaseAction;
import annotations.SetAction;
import menus.Menus;
import utils.ConnectedUser;
import utils.ConsoleHandler;

import javax.naming.NoPermissionException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

public class ActionSupplier {
  private static ConsoleHandler<String> consoleHandler = new ConsoleHandler<>();
  public static TreeMap<ActionsCode, ActionClass> userActions = new TreeMap<>();
  public static TreeMap<ActionsCode, BaseActionClass> baseActions = new TreeMap<>();

  public static void initActions() {
    for (Method method :
        ActionSupplier.combineAll(
            new Class[] {BaseActions.class, UserActions.class},
            new Class[] {BaseAction.class, SetAction.class})) {
      boolean isSetAction = method.isAnnotationPresent(SetAction.class);
      Runnable runnable =
          () -> {
            try {
              if (isSetAction) {
                method.invoke(UserActions.class);
              } else {
                method.invoke(BaseActions.class);
              }
            } catch (InvocationTargetException | IllegalAccessException e) {
              e.printStackTrace();
            }
          };
      if (isSetAction) {
        SetAction setAction = method.getAnnotation(SetAction.class);
        ActionSupplier.userActions.put(
            setAction.actionCode(), new ActionClass(setAction.permissions(), runnable));
      } else {
        BaseAction baseAction = method.getAnnotation(BaseAction.class);
        ActionSupplier.baseActions.put(baseAction.actionCode(), new BaseActionClass(runnable));
      }
    }
  }

  public static Method[] combineAll(Class<?>[] klass, Class<? extends Annotation>[] annotation) {
    List<Method> resultList = new ArrayList<>();
    Arrays.stream(klass)
        .forEach(
            aClass -> {
              for (Method method : aClass.getDeclaredMethods()) {
                if (Arrays.stream(annotation).anyMatch((method::isAnnotationPresent))) {
                  resultList.add(method);
                }
              }
            });

    Method[] resultArray = (Method[]) Array.newInstance(Method.class, resultList.size());

    return resultList.toArray(resultArray);
  }

  public static void startBase() {
    String EXIT_CODE = "EXIT";
    String option = "";

    while (!option.equals(EXIT_CODE)) {
      Menus.printWelcomingMessage();

      for (ActionsCode actionsCode : ActionSupplier.baseActions.keySet()) {
        ActionSupplier.consoleHandler.print(actionsCode.desc());
      }

      option = ConsoleHandler.stringInput();
      try {
        ActionSupplier.baseActions.get(ActionsCode.valueOf(option)).getRunnable().run();
      } catch (Exception e) {
        System.out.println("enter valid input");
      }
    }
  }

  public static void startUser() throws NoPermissionException {
    String EXIT_CODE = "EXIT";
    String option = "";
    Permissions userPermission =
        Permissions.valueOf(ConnectedUser.getInstance().getCurrUser().getJob());

    while (!option.equals(EXIT_CODE)) {
      Menus.printWelcomingMessage();

      for (ActionsCode actionsCode : ActionSupplier.userActions.keySet()) {
        if (Arrays.asList(
                ActionSupplier.userActions.get(actionsCode).getPermissions())
            .contains(userPermission)) {
          ActionSupplier.consoleHandler.print(actionsCode.desc());
        }
      }

      option = ConsoleHandler.stringInput();
      try {
        if (Arrays.asList(
                ActionSupplier.userActions.get(ActionsCode.valueOf(option)).getPermissions())
            .contains(userPermission)) {
          ActionSupplier.userActions.get(ActionsCode.valueOf(option)).getRunnable().run();
        } else {
          throw new NoPermissionException("you are not allowed to use this option");
        }
      } catch (Exception e) {
        System.out.println("enter valid input");
      }
    }
  }
}
