package learn;

import exceptions.AlreadyLearning;

public enum CurrentLearningCourse {
    INSTANCE;

    LearnCourse currentCourse;

    public LearnCourse getCurrentCourse() {
        return this.currentCourse;
    }

    public void startLearning(LearnCourse courseToAdd) throws AlreadyLearning {
        if(this.currentCourse != null) {
            throw new AlreadyLearning(currentCourse.toString());
        }

        this.currentCourse = courseToAdd;
        this.currentCourse.start();
    }

    public void stopLearning() {
        this.currentCourse.stop();
        this.currentCourse = null;
    }

    public void changeLearningCourse(LearnCourse courseToChangeTo) throws AlreadyLearning {
        this.stopLearning();
        this.startLearning(courseToChangeTo);
    }

    public boolean currentlyLearning() {
        return this.currentCourse != null;
    }
}
