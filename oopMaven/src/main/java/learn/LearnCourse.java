package learn;

import FileHandler.FileReader;
import FileHandler.FileWriter;
import com.stopwatch.IStopWatch;
import com.stopwatch.StopWatch;
import courses.Participant;
import cycle.Cycle;
import lombok.Getter;
import lombok.Setter;
import utils.ConnectedUser;

import java.util.ArrayList;

@Getter
@Setter
public class LearnCourse {
  private Cycle currentCycle;
  private IStopWatch stopwatch;

  public LearnCourse(Cycle currentCycle) {
    this.currentCycle = currentCycle;
    this.stopwatch = StopWatch.create();
  }

  public void start() {
    this.stopwatch.start();
  }

  public void stop() {

    String DB_PATH = "src/main/java/db/";
    String CYCLES_PATH = "cycles.dat";
    FileWriter<ArrayList<Cycle>> fileWriter = new FileWriter<>(DB_PATH + CYCLES_PATH);
    FileReader<ArrayList<Cycle>> fileReader = new FileReader<>(DB_PATH + CYCLES_PATH);
    ArrayList<Cycle> allCycle = fileReader.read();


    Participant currentParticipant =
            allCycle.stream().filter((cycle -> cycle.getId() == this.currentCycle.getId())).toList().get(0).
        getParticipants().stream()
            .filter(participant -> participant.getUser().getPersonalNum().equals(ConnectedUser.getInstance().getCurrUser().getPersonalNum()))
            .toList()
            .get(0);

    currentParticipant.setTimeLearned(currentParticipant.getTimeLearned() + (double)this.stopwatch.elapsedMillis() / 1000 / 60 / 60);
    this.stopwatch.stop();

    fileWriter.write(allCycle);
    this.stopwatch.restart();
  }
}
