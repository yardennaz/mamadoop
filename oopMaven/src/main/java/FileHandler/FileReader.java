package FileHandler;

import lombok.Getter;
import lombok.Setter;

import java.io.*;

@Setter
@Getter
public class FileReader<T> {
  private String filePath;

  public FileReader(String filePath) {
    this.filePath = filePath;
  }

  public T read() {
    if (new File(this.filePath).length() != 0) {
      T object = null;
      try (ObjectInputStream inputFile =
          new ObjectInputStream(new FileInputStream(this.filePath))) {
        object = (T) inputFile.readObject();
      } catch (IOException | ClassNotFoundException e) {
        e.printStackTrace();
      }
      return object;
    } else {
      return null;
    }
  }
}
