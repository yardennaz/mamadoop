package FileHandler;

import lombok.Setter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

@Setter
public class FileWriter<T> {
  private String filePath;

  public FileWriter(String filePath) {
    this.filePath = filePath;
  }

  public void write(T objToWrite) {
    try (ObjectOutputStream outputFile =
        new ObjectOutputStream(new FileOutputStream(this.filePath))) {
      outputFile.writeObject(objToWrite);

    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
