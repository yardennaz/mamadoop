package annotations;

import actions.ActionsCode;
import actions.Permissions;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface SetAction {
    Permissions[] permissions();
    ActionsCode actionCode();
}
