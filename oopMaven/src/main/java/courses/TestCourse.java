package courses;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TestCourse extends Course {
    private int minGrade;

    public TestCourse(String name, int length, String desc, int minGrade) {
        super(name, length, desc);
        this.minGrade = minGrade;
    }
}
