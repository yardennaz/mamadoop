package courses;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StepCourse extends Course{
    private String minSpecialityLevel;
    private String newLevel;

    public StepCourse(String name, int length, String desc, String minSpecialityLevel, String newLevel) {
        super(name, length, desc);
        this.minSpecialityLevel = minSpecialityLevel;
        this.newLevel = newLevel;
    }
}
