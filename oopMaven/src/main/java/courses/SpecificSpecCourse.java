package courses;

import lombok.Getter;
import lombok.Setter;
import users.SpecialityTypes;

@Getter
@Setter
public class SpecificSpecCourse extends Course{
    private SpecialityTypes[] courseSpecialities;

    public SpecificSpecCourse(String name, int length, String desc, SpecialityTypes[] courseSpecialities) {
        super(name, length, desc);
        this.courseSpecialities = courseSpecialities;
    }
}
