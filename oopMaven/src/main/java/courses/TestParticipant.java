package courses;

import lombok.Getter;
import lombok.Setter;
import users.User;

@Getter
@Setter
public class TestParticipant extends Participant {
    private int grade;

    public TestParticipant(User user, int grade) {
        super(user);
        this.grade = grade;
    }
}
