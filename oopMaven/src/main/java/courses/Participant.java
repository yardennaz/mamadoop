package courses;

import lombok.Getter;
import lombok.Setter;
import users.User;

import java.io.Serializable;

@Getter
@Setter
public class Participant implements Serializable {
    private final long serialVersionUID = 1L;

    private User user;
    private double timeLearned;
    private boolean didPass;

    public Participant(User user) {
        this.user = user;
        this.didPass = false;
        this.timeLearned = 0;
    }
}
