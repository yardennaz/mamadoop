package courses;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter

public abstract class Course implements Serializable {

    private final long serialVersionUID = 1L;
    private String name;
    private int length;
    private String desc;

    public Course(String name, int length, String desc) {
        this.name = name;
        this.length = length;
        this.desc = desc;
    }

    @Override
    public String toString() {
        return "Course:\n" +
                "name='" + name + '\'' +
                ", length=" + length +
                ", desc='" + desc + '\'';
    }
}
