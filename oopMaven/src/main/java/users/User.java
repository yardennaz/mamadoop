package users;

import java.io.Serializable;
import java.util.Objects;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class User implements Serializable, Cloneable {

  private final long serialVersionUID = 1L;
  private String job;
  private String personalNum;
  private String passHash;
  private Speciality speciality;
  private String name;

  public User(String personalNum, String passHash, SpecialityTypes specType, String specLevel, String name, String jobName) {
    this.personalNum = personalNum;
    this.passHash = passHash;
    this.speciality = new Speciality(specType, specLevel);
    this.name = name;
    this.job = jobName;
  }

  public User() {
    this.personalNum = "";
    this.passHash = "";
    this.name = "";
  }

  public Object clone() throws CloneNotSupportedException {
    return super.clone();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    User user = (User) o;
    return Objects.equals(personalNum, user.personalNum)
        && Objects.equals(passHash, user.passHash)
        && Objects.equals(speciality, user.speciality)
        && Objects.equals(name, user.name);
  }

  public void print() {}
}
