package users;

public enum SpecialityTypes {
    TIHNUT("TIHNUT"),
    BODEK("BODEK"),
    DEVOPS("DEVOPS");

    private String name;

    SpecialityTypes(String name) {
        this.name = name;
    }
}
