package users;

import lombok.Getter;

@Getter
public class Student extends User {
  private final String jobName = "STUDENT";

  public Student(
      String personalNum, String password, String name, SpecialityTypes type, String level, String job) {
    super(personalNum, password, type, level, name, job);
  }

  public Student() {
    super();
  }

  @Override
  public void print() {
    System.out.println(this.getName());
  }
}
