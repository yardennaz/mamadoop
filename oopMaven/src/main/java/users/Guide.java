package users;
import lombok.Getter;

@Getter
public class Guide extends User {
  public Guide(String personalNum, String password, String name, SpecialityTypes type, String level, String job) {
    super(personalNum, password,type, level, name, job);
  }

  public Guide() {
    super();
  }

  @Override
  public void print() {
    System.out.println(this.getName());
  }
}
