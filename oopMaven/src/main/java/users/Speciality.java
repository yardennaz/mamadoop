package users;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter @Setter
public class Speciality implements Serializable {
    private final long serialVersionUID = 1L;
    private SpecialityTypes type;
    private String level;

    public Speciality(SpecialityTypes type, String level) {
        this.type = type;
        this.level = level;
    }
}
