package users;

import lombok.Getter;

@Getter
public class Manager extends User {
  private final String jobName = "MANAGER";

  public Manager(String personalNum, String password, String name, SpecialityTypes type, String level, String job) {
    super(personalNum, password,type, level, name, job);
  }

  public Manager() {
    super();
  }

  @Override
  public void print() {
    System.out.println(this.getName());
  }
}
