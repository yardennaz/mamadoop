package utils;

import FileHandler.FileReader;
import exceptions.InvalidParameters;
import userFactory.UserFactory;
import users.User;

import java.util.ArrayList;


public class ConnectedUser {
  private static final UserFactory userFactory = UserFactory.getInstance();
  private static ConnectedUser instance = null;

  private User currUser;

  public User getCurrUser() {
    return this.currUser;
  }

  public void setCurrUser(User user) throws CloneNotSupportedException {
    this.currUser = (User) user.clone();
  }

  public static ConnectedUser getInstance() {
    if (ConnectedUser.instance == null) {
      ConnectedUser.instance = new ConnectedUser();
    }

    return ConnectedUser.instance;
  }

  public void attemptConnection(String personalNum, String password)
      throws CloneNotSupportedException, InvalidParameters {
    FileReader<ArrayList<User>> fileReader = new FileReader<>("src/main/java/db/users.dat");
    ArrayList<User> allUsers = fileReader.read();
    String passHash = CreateHash.toHash(password);
    User currUser =  allUsers.stream()
            .filter(
                    (user -> user.getPersonalNum().equals(personalNum)
                            && user.getPassHash().equals(passHash))).toList().get(0);
    if(currUser == null) {
      throw new InvalidParameters("the password did not match the soldier id");
    }
    this.currUser = (User) currUser.clone();
  }
}
