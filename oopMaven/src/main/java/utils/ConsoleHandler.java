package utils;

import java.util.Scanner;

public class ConsoleHandler<T> {
  private static final Scanner userInput = new Scanner(System.in);

  public static String stringInput() {
    return ConsoleHandler.userInput.nextLine();
  }

  public void print(T output) {
    System.out.println(output);
  }
}
