package utils;

import com.google.common.hash.Hashing;

import java.nio.charset.StandardCharsets;

public class CreateHash {
    public static String toHash(String string) {
        return Hashing.sha256()
                .hashString(string, StandardCharsets.UTF_8)
                .toString();
    }
}
