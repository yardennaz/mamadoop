package exceptions;

public class AlreadyLearning extends Exception{
    public AlreadyLearning(String message) {
        super("cant start to learn a new course because use is already learning: " + message);
    }
}
