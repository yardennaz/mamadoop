package exceptions;

public class InvalidParameters extends Exception{
    public InvalidParameters(String message) {
        super("parameters exception: " +
                message);
    }
}
